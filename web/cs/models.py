from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from urllib.parse import urlparse, urljoin
from model_utils import FieldTracker
from django.db.models import Count, F, Value
from .tasks import start_twitter_scan
from .validators import *
from .parse import monthdelta, file_log

# Create your models here.

exposed_request = ""

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile', default=None, null=True, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    twitter_account = models.TextField(max_length=255, null=True, blank=True, validators=[twitter_validator])

    TWITTER_REQUIREMENTS = {
        "twitter_followers": 300,
        "twitter_age": 12,
        "twitter_likes_and_comments": 100
    }



    twitter_followers = models.IntegerField(default=0)

    twitter_register_date = models.DateTimeField(null=True, blank=True)
    #twitter_likes_and_comments = models.IntegerField(default=0)
    twitter_total_posts = models.IntegerField(default=0)
    twitter_total_likes = models.IntegerField(default=0)
    twitter_total_comments = models.IntegerField(default=0)
    twitter_last_parse_date = models.DateTimeField(null=True, blank=True)

    facebook_account = models.TextField(max_length=255, null=True, blank=True)
    eth_wallet = models.CharField(max_length=255, null=True, blank=True)
    balance = models.FloatField(default=0)

    SCAN_STATUS = (
        (-1, 'Account declined'),
        (0, 'Not scanning'),
        (1, 'Scanning followers'),
        (2, 'Scanning profile age'),
        (3, 'Scanning avarage likes and comments'),
        (4, 'Account accepted'),
    )
    scan_process = models.IntegerField(choices=SCAN_STATUS, default=0, verbose_name="Стадия сканирования")

    TYPES = (
        (1, 'Influencer'),
        (2, 'Advertiser'),
    )
    profile_type = models.IntegerField(choices=TYPES, default=1, verbose_name="Тип профиля")

    
    def getMultiplier(self, amount):
        return round(amount * self.getCleanMultiplier, 2)

    @property
    def getCleanMultiplier(self):
        multi = 1

        multi = multi * (self.twitter_followers / self.TWITTER_REQUIREMENTS['twitter_followers'])

        if self.getTwitterAge >= 24:
            
            multi = multi * (self.getTwitterAge / 24)

        if self.getTwitterSumOfRepliesAndLikes >= (self.TWITTER_REQUIREMENTS['twitter_likes_and_comments'] * 2):
            multi = multi * (self.getTwitterSumOfRepliesAndLikes / (self.TWITTER_REQUIREMENTS['twitter_likes_and_comments'] * 2))

        return round(multi,2)



    @property
    def getTwitterAge(self):
        from dateutil import relativedelta
        from django.utils import timezone
        import datetime

        if not self.twitter_register_date:
            return 0

        today = timezone.now()
        months = int((today - self.twitter_register_date).days / 30)
        return months

    @property
    def isTwitterOk(self):
        
        ok1 = self.getTwitterAge >= self.TWITTER_REQUIREMENTS['twitter_age']
        ok2 = self.twitter_followers >= self.TWITTER_REQUIREMENTS['twitter_followers']
        ok3 = self.getTwitterSumOfRepliesAndLikes >= self.TWITTER_REQUIREMENTS['twitter_likes_and_comments']

        if not ok1 or not ok2 or not ok3:
            return False

        return True

    @property
    def getTwitterSumOfRepliesAndLikes(self):
        summ = self.twitter_total_likes + self.twitter_total_comments
        av = int(summ / (self.twitter_total_posts or 1))

        return av


    def getTwitterFollowersCount(self):
        import re
        from .parse import get_selenium_contents, get_driver
        from selenium import webdriver
        import datetime
        
        
        twitter = self.twitter_account
        

        count = 0

        chrome = get_driver()

        try:
            els = get_selenium_contents(chrome, twitter, "[data-nav='followers'] [data-count]", login=False)

            for element in els:
                
                count = int(element.get_attribute('data-count'))
                self.twitter_followers = count
                self.save()
                break

            


        finally: 
            chrome.close()
            chrome.quit()

    def getAverageLikesAndComments(self):
        import re
        from .parse import get_selenium_contents, get_driver
        from selenium import webdriver
        import datetime
        from bs4 import BeautifulSoup
        
        twitter = self.twitter_account
        

        count = 0

        chrome = get_driver()

        time_compare = monthdelta(datetime.datetime.now(), -3)
        total_likes = 0
        total_comments = 0
        total_posts = 0

        try:
            els = get_selenium_contents(chrome, twitter, "a > time", scroll_to_bottom=50000000, time_compare=time_compare)

            total_posts = len(els)
            print("Total posts: {}".format(total_posts))

            for element in els:
                
                #file_log("element.html", element)
                soup = BeautifulSoup(element)

                group = soup.select('[role="group"]').pop()
                if group:
                    regex = r"(?P<replies>\d+ rep[a-zA-Z]+)|(?P<retweets>\d+ retw[a-zA-Z]+)|(?P<likes>\d+ lik[a-zA-Z]+)"
                    test_str = group.get("aria-label", "").lower()
                    

                    arr = list(re.finditer(regex, test_str))

                    s_arr = ["retweets", "replies", "likes"]
                    new_obj = {}

                    for item in arr:
                        gd = item.groupdict()
                        
                        for itm in s_arr:
                            if gd[itm]:
                                new_obj[itm] = int(re.sub("[a-zA-Zа-яА-Я ]", "", gd[itm]))

                    for itm in s_arr:
                        if itm not in new_obj:
                            new_obj[itm] = 0

                    total_likes+=new_obj['likes']
                    total_comments+=new_obj['replies']

            print("Total posts: {}, Total likes: {}, Total comments: {}".format(total_posts, total_likes, total_comments))
            self.twitter_last_parse_date = timezone.now()
            self.twitter_total_comments = total_comments
            self.twitter_total_likes = total_likes
            self.twitter_total_posts = total_posts
            self.save()


        finally: 
            chrome.close()
            chrome.quit()

    def getTwitterRegisterDate(self):
        import re
        from .parse import get_selenium_contents, get_driver
        from selenium import webdriver
        import datetime
        
        
        twitter = self.twitter_account
        chrome = get_driver()

        try:
            els = get_selenium_contents(chrome, twitter, "span.ProfileHeaderCard-joinDateText", login=False)
            joined = 0

            for element in els:
                html = element.get_attribute('innerHTML')
                
                regex = r"Joined (\S+\s\S+)"

                matches = re.finditer(regex, html)

                for matchNum, match in enumerate(matches, start=1):
                    for groupNum in range(0, len(match.groups())):
                        groupNum = groupNum + 1
                        
                        joined = match.group(groupNum)
                        break

                if joined:
                    joined = datetime.datetime.strptime(joined, '%B %Y').date()
                    self.twitter_register_date = joined
                    self.save()
                    break



                

        finally: 
            chrome.close()
            chrome.quit()


    def startTwitterScan(self):
        if self.scan_process == 0 or self.scan_process == -1 or self.scan_process == 4:
            start_twitter_scan.delay(self.id)

    def charge(self, amount):
        tr = Transaction.objects.create(user=self.user, amount=amount)
        return tr


    def getTwitterAccountName(self):
        url = urlparse(self.twitter_account).path
        return url


    def __str__(self):
        return self.user.username

@receiver(models.signals.post_save, sender=User)
def after_save_user(sender, instance, created=False, **kwargs):
    profile = UserProfile.objects.filter(pk=instance.id).exists()
    if not profile:
        UserProfile.objects.create(user=instance, pk=instance.id)

class Transaction(models.Model):
    user = models.ForeignKey(User, related_name='transactions', on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    amount = models.FloatField(default=0)
    multiplied_amount = models.FloatField(default=0)


    def __str__(self):
        return "{} -> {}".format(self.amount, self.user.username)

@receiver(models.signals.post_save, sender=Transaction)
def after_save_transaction(sender, instance, created=False, **kwargs):
    
    if created:

        amount = instance.amount
        amount = instance.user.profile.getMultiplier(amount)
        instance.multiplied_amount = amount
        instance.save()
        UserProfile.objects.filter(pk=instance.user.id).update(balance=F('balance')+amount)
        

class Campaign(models.Model):
    campaign_name = models.CharField(max_length = 32)
    campaign_description = models.TextField(max_length = 256, null=True, blank=True, default='')

    campaign_location = models.CharField(null=True, blank=True, max_length = 256)

    SEX = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    campaign_sex = models.CharField(choices=SEX, null=True, blank=True, max_length=1)
    campaign_age_from = models.IntegerField(null=True, blank=True)
    campaign_age_to = models.IntegerField(null=True, blank=True)
    campaign_occupation = models.CharField(null=True, blank=True, max_length = 256)

    creator = models.ForeignKey(User, related_name="created_campaigns", null=True, blank=True, on_delete=models.CASCADE)
    #users = models.ManyToManyField(User, blank=True, null=True)

    def quit_campaign(self, user):

        u_c, created = Users_Campaigns.objects.get_or_create(campaign=self, user=user)
        
        u_c.active = False
        u_c.save()
        return self

    def join_campaign(self, user):

        u_c, created = Users_Campaigns.objects.get_or_create(campaign=self, user=user)
        
        u_c.active = True
        u_c.save()

        u_c.check_compaign_post_results(user)
        return self

    class Meta:
        unique_together = [['creator', 'campaign_name']]



    

    def __str__(self):
        return self.campaign_name

class Users_Campaigns(models.Model):
    # def __init__(self, user, campaign):
    #     super().__init__()
    # user = models.ManyToManyField(User, db_index=True, on_delete=models.CASCADE)
    #campaigns = models.ManyToManyField(User, db_index=True)
    user = models.ForeignKey(User, related_name="campaigns", on_delete=models.PROTECT)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)

    def check_compaign_post_results(self, user):

        social = 'tw'

        for post in self.campaign.posts.all():
            if post.like_price > 0:
                status, created = PostShareStatus.objects.get_or_create(post=post, user_campaign=self, user=user, social=social, share_type=2)
                

            if post.retweet_price > 0:
                status, created = PostShareStatus.objects.get_or_create(post=post, user_campaign=self, user=user, social=social, share_type=3)
                

            if post.share_price > 0:
                status, created = PostShareStatus.objects.get_or_create(post=post, user_campaign=self, user=user, social=social, share_type=1)
                

            if post.comment_price > 0:
                status, created = PostShareStatus.objects.get_or_create(post=post, user_campaign=self, user=user, social=social, share_type=4)
                

            if post.follow_price > 0:
                status, created = PostShareStatus.objects.get_or_create(post=post, user_campaign=self, user=user, social=social, share_type=5)
                

        return self

    class Meta:
        unique_together = [['user', 'campaign']]

    # def __str__(self):
    #     return self.user

class UserSocials(models.Model):
    user = models.OneToOneField(User, default=None, null=True, on_delete=models.CASCADE)
    user_twitter = models.TextField()
    user_facebook = models.TextField()
    user_tw_followers = models.IntegerField(blank=True, null=True)
    user_tw_reg_date = models.CharField(max_length = 9, blank=True, null=True)
    user_tw_likes =  models.BigIntegerField(blank=True, null=True)

class Post(models.Model):
    post_link = models.TextField(blank=True, null=True, default='')
    post_text = models.TextField(blank=True, null=True, default='')
    post_tags = models.TextField(blank=True, default='')
    TYPES = (
        (1, 'Article'),
        (2, 'Post'),
        (3, 'Follow')
    )
    follow_price = models.IntegerField(default=0)
    like_price = models.IntegerField(default=0)
    retweet_price = models.IntegerField(default=0)
    share_price = models.IntegerField(default=0)
    comment_price = models.IntegerField(default=0)

    post_type = models.IntegerField(choices=TYPES, default=1, verbose_name='Тип поста')
    
    campaign = models.ForeignKey(Campaign, related_name="posts", on_delete=models.CASCADE)

    ACTION_TYPES = (
        (1, 'Share'),
        (2, 'Like'),
        (3, 'Retweet'),
        (4, 'Comment'),
        (5, 'Follow')
    )

    @property
    def getPrice(self):
        prices = [
            self.follow_price,
            self.like_price,
            self.retweet_price,
            self.share_price,
            self.comment_price
        ]

        return max(prices)

    @property
    def getActionType(self):
        typ = 0
        if self.follow_price > 0:
            typ = 5
        if self.like_price > 0:
            typ = 2
        if self.retweet_price > 0:
            typ = 3
        if self.share_price > 0:
            typ = 1
        if self.comment_price > 0:
            typ = 4

        return typ

    @property
    def get_share_status(self):
        status = self.statuses.filter(user=exposed_request.user, share_type=1).first()

        if status:
            return status.status
        else:
            return 0

    @property
    def get_like_status(self):
        status = self.statuses.filter(user=exposed_request.user, share_type=2).first()

        if status:
            return status.status
        else:
            return 0

    @property
    def get_retweet_status(self):
        status = self.statuses.filter(user=exposed_request.user, share_type=3).first()

        if status:
            return status.status
        else:
            return 0

    @property
    def get_comment_status(self):
        status = self.statuses.filter(user=exposed_request.user, share_type=4).first()

        if status:
            return status.status
        else:
            return 0

    @property
    def get_follow_status(self):
        status = self.statuses.filter(user=exposed_request.user, share_type=5).first()

        if status:
            return status.status
        else:
            return 0

    def __str__(self):
        return self.post_link or "id{}".format(self.id)

class PostShareStatus(models.Model):
    post = models.ForeignKey(Post, null=True, related_name='statuses', blank=True, on_delete=models.CASCADE)
    user_campaign = models.ForeignKey(Users_Campaigns, null=True, related_name='post_share_statuses', blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, default=None, null=True, on_delete=models.CASCADE)
    SOCIALS = (
        ('tw', 'Twitter'),
        ('fb', 'Facebook'),
    )
    social = models.CharField(max_length=2, choices=SOCIALS, default='tw', verbose_name='Тип сети')

    TYPES = (
        (1, 'Share'),
        (2, 'Like'),
        (3, 'Retweet'),
        (4, 'Comment'),
        (5, 'Follow')
    )
    share_type = models.IntegerField(choices=TYPES, default=0, verbose_name='Тип действия')
    STATUSES = (
        (0, 'Не делился'),
        (1, 'Проверка'),
        (2, 'Поделился')
    )
    date = models.DateTimeField(null=True, blank=True, verbose_name='Дата обновления статуса')
    status = models.IntegerField(choices=STATUSES, default=0, verbose_name='Статус')

    tracker = FieldTracker()

    def check_submit(self):
        from .parse import scrape
        twitter = self.user.profile.twitter_account
        facebook = self.user.profile.facebook_account
        self.status = 1
        self.save()

        if self.social == "tw":
            soup = scrape(twitter)
            titles = soup.select(".js-tweet-text-container p")

            for title in titles:
                text = title.decode_contents()
                print("Checking {}".format(text))
                if text.rstrip().find(self.post.post_text.rstrip()) != -1:
                    self.status = 2
                    self.save()
                    break

        if self.status == 1:
            self.status = 0
            self.save()

    def check_liked(self):
        from .parse import get_selenium_contents, get_driver
        twitter = self.user.profile.twitter_account
        self.status = 1
        self.save()

        from selenium import webdriver
        

        chrome = get_driver()

        try:

            post_link = self.post.post_link
            if post_link[-1] != "/":
                post_link+="/"
            likes_page = urljoin(post_link, 'likes')
            print("Like page is "+likes_page)
            compare = self.user.profile.getTwitterAccountName().lower()
            scroll_element = 'document.querySelector("section[aria-labelledby^=acces]").parentNode.parentNode'

            if self.social == "tw":
                #soup = scrape(likes_page)
                els = get_selenium_contents(chrome, likes_page, "[aria-labelledby] [data-testid='UserCell'] [aria-haspopup]", scroll_element=scroll_element, scroll_to_bottom=50000000, compare=compare)

                for user in els:
                    link = urlparse(user.get_attribute('href')).path.lower()
                    
                    

                    print("Link is {link} comparing to {compare}".format(link=link, compare=compare))
                    if not link:
                        continue

                    
                    if link == compare:
                        self.status = 2
                        self.save()
                        break
        finally: 
            chrome.close()
            chrome.quit()
            if self.status == 1:
                self.status = 0
                self.save()

    def check_retweeted(self):
        from .parse import get_selenium_contents, get_driver
        twitter = self.user.profile.twitter_account
        self.status = 1
        self.save()

        from selenium import webdriver
        chrome = get_driver()

        try:

            post_link = self.post.post_link
            if post_link[-1] != "/":
                post_link+="/"
            likes_page = urljoin(post_link, 'retweets')
            print("Retweets page is "+likes_page)
            compare = self.user.profile.getTwitterAccountName().lower()
            scroll_element = 'document.querySelector("section[aria-labelledby^=acces]").parentNode.parentNode'

            if self.social == "tw":
                #soup = scrape(likes_page)
                els = get_selenium_contents(chrome, likes_page, "[aria-labelledby] [data-testid='UserCell'] [aria-haspopup]", scroll_element=scroll_element, scroll_to_bottom=50000000, compare=compare)

                for user in els:
                    link = urlparse(user.get_attribute('href')).path.lower()
                    
                    

                    print("Link is {link} comparing to {compare}".format(link=link, compare=compare))
                    if not link:
                        continue

                    
                    if link == compare:
                        self.status = 2
                        self.save()
                        break
        finally: 
            chrome.close()
            chrome.quit()
            if self.status == 1:
                self.status = 0
                self.save()

    def check_commented(self):
        from .parse import get_selenium_contents, get_driver
        twitter = self.user.profile.twitter_account
        self.status = 1
        self.save()

        from selenium import webdriver
        chrome = get_driver()

        try:

            post_link = self.post.post_link
            if post_link[-1] != "/":
                post_link+="/"
            likes_page = post_link
            print("Comments page is "+likes_page)
            compare = self.user.profile.getTwitterAccountName().lower()

            if self.social == "tw":
                #soup = scrape(likes_page)
                els = get_selenium_contents(chrome, likes_page, "article a[aria-haspopup='false']", scroll_to_bottom=50000000, compare=compare)

                for user in els:

                    try:
                        link = urlparse(user.get_attribute('href')).path.lower()
                    except:
                        continue
                    
                    

                    print("Link is {link} comparing to {compare}".format(link=link, compare=compare))
                    if not link:
                        continue

                    
                    if link == compare:
                        self.status = 2
                        self.save()
                        break
        finally: 
            chrome.close()
            chrome.quit()
            if self.status == 1:
                self.status = 0
                self.save()

    def check_followed(self):
        from .parse import get_selenium_contents, get_driver
        twitter = self.user.profile.twitter_account
        self.status = 1
        self.save()

        from selenium import webdriver
        chrome = get_driver()

        try:

            post_link = self.post.post_link
            if post_link[-1] != "/":
                post_link+="/"
            likes_page = urljoin(post_link, 'followers')
            print("Followers page is "+likes_page)
            compare = self.user.profile.getTwitterAccountName().lower()
            compare_selector = "[aria-labelledby] [data-testid='UserCell'] [aria-haspopup]"#.format(compare)

            if self.social == "tw":
                #soup = scrape(likes_page)
                print("Compare selector is "+compare_selector)
                els = get_selenium_contents(chrome, likes_page, compare_selector, scroll_to_bottom=50000000, compare=compare)

                for user in els:
                    link = urlparse(user.get_attribute('href')).path.lower()
                    

                    print("Link is {link} comparing to {compare}".format(link=link, compare=compare))
                    if not link:
                        continue

                    
                    if link == compare:
                        self.status = 2
                        self.save()
                        break
        finally: 
            chrome.close()
            chrome.quit()
            if self.status == 1:
                self.status = 0
                self.save()



    def __str__(self):
        for item in self.TYPES:
            if item[0] == self.share_type:
                st = item[1]
                break

        for item in self.post.TYPES:
            if item[0] == self.post.post_type:
                pt = item[1]
                break

        return "{} -> {} -> {} -> {}".format(self.user.username, pt, st, self.post.post_link)

@receiver(models.signals.post_save, sender=PostShareStatus)
def after_save_post_status(sender, instance, created=False, **kwargs):
    instance.date = timezone.now()

    if created:
        pass
    else:
        if instance.tracker.has_changed('status'):
            if instance.status == 2:

                if instance.share_type == 1:
                    amount = instance.post.share_price

                if instance.share_type == 2:
                    amount = instance.post.like_price

                if instance.share_type == 3:
                    amount = instance.post.retweet_price

                if instance.share_type == 4:
                    amount = instance.post.comment_price

                if instance.share_type == 5:
                    amount = instance.post.follow_price

                instance.user.profile.charge(amount)



