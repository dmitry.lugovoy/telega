from django.contrib import admin
from cs.models import *

# Register your models here.

#admin.site.register(User)
admin.site.register(Post)
admin.site.register(PostShareStatus)
admin.site.register(UserProfile)
admin.site.register(Campaign)
admin.site.register(Users_Campaigns)
admin.site.register(Transaction)