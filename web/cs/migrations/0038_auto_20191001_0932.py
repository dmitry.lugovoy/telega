# Generated by Django 2.2.3 on 2019-10-01 09:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cs', '0037_auto_20191001_0906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='post_link',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_text',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
