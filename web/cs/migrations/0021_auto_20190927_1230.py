# Generated by Django 2.2.3 on 2019-09-27 12:30

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cs', '0020_users_campaigns_active'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='users_campaigns',
            unique_together={('user', 'campaign')},
        ),
    ]
