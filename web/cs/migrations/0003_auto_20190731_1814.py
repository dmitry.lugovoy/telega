# Generated by Django 2.2.3 on 2019-07-31 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cs', '0002_auto_20190730_0002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersocials',
            name='user_tw_likes',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
