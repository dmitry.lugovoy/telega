# Generated by Django 2.2.3 on 2019-08-28 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cs', '0008_auto_20190828_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='postsharestatus',
            name='social',
            field=models.CharField(choices=[('tw', 'Twitter'), ('fb', 'Facebook')], default='tw', max_length=2, verbose_name='Тип сети'),
        ),
    ]
