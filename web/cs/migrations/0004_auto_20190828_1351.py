# Generated by Django 2.2.3 on 2019-08-28 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cs', '0003_auto_20190731_1814'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='post_tags',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='post',
            name='post_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_link',
            field=models.TextField(blank=True, null=True),
        ),
    ]
