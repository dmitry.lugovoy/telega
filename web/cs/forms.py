from django import forms
from cs.models import UserSocials
from django.forms import ModelForm, Textarea
from django.contrib.auth.models import User
from .models import UserProfile, Post, Campaign
from .validators import *
import json

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password')

class PostForm(forms.Form):

    post_id = forms.CharField(required=False)
    post_price = forms.IntegerField(required=False)
    share_price = forms.IntegerField(required=False)
    like_price = forms.IntegerField(required=False)
    retweet_price = forms.IntegerField(required=False)
    comment_price = forms.IntegerField(required=False)
    follow_price = forms.IntegerField(required=False)
    post_link = forms.CharField(required=False)
    post_text = forms.CharField(required=False)
    post_tags = forms.CharField(required=False)
    post_type = forms.ChoiceField(choices=Post.ACTION_TYPES, required=False)

    def clean_post_link(self):
        data = self.cleaned_data
       
        if len(data['post_link'].rstrip()) == 0:
            raise forms.ValidationError("Action source must not be blank")
        
        return data['post_link']

    def clean_post_id(self):
        data = self.cleaned_data
       
        if data['post_id'] == "None":
            data['post_id'] = 0
        else:
            data['post_id'] = int(data['post_id'])

        
        return data['post_id']


   

    def clean_post_text(self):
        data = self.cleaned_data
        raw_data = self.data

        if int(raw_data['post_type']) == 1:
            if len(data['post_text'].rstrip()) == 0:
                raise forms.ValidationError("Post text must not be blank with that action type")

        return data['post_text']

    def clean(self):
        data = self.data
        cd = self.cleaned_data
        

        post_type = int(data['post_type'])

        

        if post_type == 1:
            cd['post_type'] = 1
        elif post_type == 5:
            cd['post_type'] = 3
        else:
            cd['post_type'] = 2


        
        if int(data['post_price']) <= 0:
            raise forms.ValidationError({"post_price":"Reward must be positive value"})

        

        return self.cleaned_data

    

    # class Meta():
    #     model = Post
    #     fields = ('post_link','post_text','post_tags','post_type')


class CampaignForm(forms.Form):

    actions = forms.CharField(required=True)
    campaign_name = forms.CharField(required=True)
    campaign_description = forms.CharField(required=False, max_length = 256)
    campaign_location = forms.CharField(required=False, max_length = 256)
    campaign_sex = forms.ChoiceField(choices=Campaign.SEX, required=False)
    campaign_age_from = forms.IntegerField(required=False)
    campaign_age_to = forms.IntegerField(required=False)
    campaign_occupation = forms.CharField(required=False, max_length = 256)

    def clean_actions(self):
        value = self.cleaned_data.get('actions', '').rstrip()
        actions = json.loads(value)
        errors = {}
        errors_exists = False
        out_actions = []

        if len(actions) == 0:
            raise forms.ValidationError("You must enter at least one action")

        for action in actions:
            action['share_price'] = 0
            action['like_price'] = 0
            action['retweet_price'] = 0
            action['comment_price'] = 0
            action['follow_price'] = 0

            if int(action['post_type']) == 1:
                action['share_price'] = action['post_price']

            if int(action['post_type']) == 2:
                action['like_price'] = action['post_price']

            if int(action['post_type']) == 3:
                action['retweet_price'] = action['post_price']

            if int(action['post_type']) == 4:
                action['comment_price'] = action['post_price']

            if int(action['post_type']) == 5:
                action['follow_price'] = action['post_price']

            #del(action['post_price'])

            post_form = PostForm(data=action)
            if not post_form.is_valid():
                errors_exists = True
                js = json.loads(post_form.errors.as_json())
                js['index'] = action['index']
                js = json.dumps(js)
                errors[action['index']] = str(json.loads(js))
            else:
                out_actions.append(post_form)


        if errors_exists:
            raise forms.ValidationError([errors])
        else:
            return out_actions




        
    

class UserProfileForm(forms.ModelForm):

    twitter_account = forms.CharField(validators=[twitter_validator])
    eth_wallet = forms.CharField(required=True)

    def clean_twitter_account(self):
       
        return self.cleaned_data.get('twitter_account', '').rstrip()

    def clean_eth_wallet(self):
       
        return self.cleaned_data.get('eth_wallet', '').rstrip()

    
   
    class Meta():
        model = UserProfile
        fields = ('twitter_account','eth_wallet')

class UserProfileFormETHOnly(forms.ModelForm):

    
    eth_wallet = forms.CharField(required=True)


    def clean_eth_wallet(self):
       
        return self.cleaned_data.get('eth_wallet', '').rstrip()

    
   
    class Meta():
        model = UserProfile
        fields = ('eth_wallet',)

class UserSocialsForm(forms.ModelForm):
    class Meta():
        model = UserSocials
        fields = ('user_twitter', 'user_facebook', 'user_tw_followers', 'user_tw_reg_date', 'user_tw_likes')
        widgets = {
            'user_twitter': Textarea(attrs={'cols': 40, 'rows': 1}),
            'user_facebook': Textarea(attrs={'cols': 40, 'rows': 1}),
        }
