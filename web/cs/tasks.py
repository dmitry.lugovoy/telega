from crypto_social.celery import app

from .parse import *
from django.db.models import F, Func, Q
from django.utils import timezone
import time

@app.task
def task_check_shared():
    from .models import PostShareStatus

    statuses = PostShareStatus.objects.filter(status=0, post__post_type=1, user_campaign__active=1)

    for status in statuses:
        status.check_submit()

@app.task
def task_check_liked():
    from .models import PostShareStatus

    statuses = PostShareStatus.objects.filter(status=0, share_type=2, user_campaign__active=1)

    for status in statuses:
        status.check_liked()


@app.task
def task_check_retweeted():
    from .models import PostShareStatus
    statuses = PostShareStatus.objects.filter(status=0, share_type=3, user_campaign__active=1)

    for status in statuses:
        status.check_retweeted()

@app.task
def task_check_commented():
    from .models import PostShareStatus
    statuses = PostShareStatus.objects.filter(status=0, share_type=4, user_campaign__active=1)

    for status in statuses:
        status.check_commented()

@app.task
def task_check_followed():
    from .models import PostShareStatus
    statuses = PostShareStatus.objects.filter(status=0, share_type=5, user_campaign__active=1)

    for status in statuses:
        status.check_followed()

@app.task
def start_twitter_scan(user):
	from .models import UserProfile
	user = UserProfile.objects.get(pk=user)
	user.twitter_followers = 0
	user.twitter_register_date = None
	user.twitter_total_posts = 0
	user.twitter_total_likes = 0
	user.twitter_total_comments = 0

	user.scan_process = 1
	user.save()
	user.getTwitterFollowersCount()
	user.refresh_from_db()

	user.scan_process = 2
	user.save()
	user.getTwitterRegisterDate()
	user.refresh_from_db()

	user.scan_process = 3
	user.save()
	user.getAverageLikesAndComments()
	user.refresh_from_db()

	if user.isTwitterOk:
		user.scan_process = 4
		user.save()
	else:
		user.scan_process = -1
		user.save()




	# for x in range(1,4):
	# 	user.scan_process = x
	# 	user.save()

	# 	time.sleep(5)

	# user.scan_process = -1
	# user.save()




	
