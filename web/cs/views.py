from django.shortcuts import render
from cs.forms import UserForm, UserSocialsForm, UserProfileForm, UserProfileFormETHOnly, CampaignForm
from cs.models import Campaign, Post, Users_Campaigns, UserSocials, User, PostShareStatus
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .parse import parse_tweet, update_from_dict
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from bs4 import BeautifulSoup
import json
from .decorators import *
import time

# Create your views here.
@check_submit_profile
def index(request):
    user = User.objects.filter(id = request.user.id)
    if not user:
        user_unknown = False
    else:
        user_unknown = True

    return render(request,'index.html', {'user_log':user_unknown})

@check_submit_profile
@login_required
def profile(request):
    user = User.objects.filter(id = request.user.id)
    if not user:
        return HttpResponse("You are not logged in !")
    else:
        # user_social = UserSocials.objects.filter(user = user[0])
        # if not user_social:
        #     noSocials = True
        #     return render(request, 'profile.html', {'noSocials':noSocials})
        # else:
        #     user_campaigns = Users_Campaigns.objects.filter(user = user[0])
        #     campaigns = []
        #     for camp in user_campaigns:
        #         campaigns.append(Campaign.objects.get(id = camp.campaign_id))
        #     twitter = user_social[0].user_twitter
        #     facebook = user_social[0].user_facebook
        #     tw_followers = user_social[0].user_tw_followers
        #     tw_likes = user_social[0].user_tw_likes
        #     tw_reg_date = user_social[0].user_tw_reg_date

        return render(request, 'profile.html',{'user':request.user})


@check_submit_profile
@login_required
def update_profile(request):
    user = User.objects.filter(id = request.user.id)
    errors = {}
    if not user:
        return HttpResponse("You are not logged in !")
    else:


        if request.method == 'POST':
            user_form = UserProfileFormETHOnly(data = request.POST, instance=request.user.profile)
            if user_form.is_valid():
                user_form.save()
                
                
            else:
                errors = user_form.errors
                


        return render(request, 'update_profile.html',{'user':request.user, 'errors': errors})

@check_submit_profile
@login_required
def reset_twitter(request):
    user = User.objects.filter(id = request.user.id)
    if not user:
        return HttpResponse("You are not logged in !")
    else:
        request.user.profile.twitter_account = None
        request.user.profile.scan_process = 0
        request.user.profile.save()

        return HttpResponseRedirect(reverse("profile_submit"))

@login_required
def profile_submit(request):
    if request.method == 'POST':
        user_form = UserProfileForm(data = request.POST, instance=request.user.profile)
        if user_form.is_valid():
            user_form.save()
            request.user.profile.startTwitterScan()
            return JsonResponse({
                "success": True,
                })
        else:
            return JsonResponse({
                "success": False,
                "errors": user_form.errors
            }, status=400)


    return render(request, 'profile_submit_twitter.html', {})

def submit_process_partial(request):

    profile = request.user.profile

    scanning_in_progress = False
    scan_percent = 0
    total_stages = 4
    scan_stage = ""

    if profile.scan_process > 0:
        scanning_in_progress = True

        for item in profile.SCAN_STATUS:
            if item[0] == profile.scan_process:
                scan_stage = item[1]
                break

        scan_percent = int(profile.scan_process / total_stages * 100)

    errors = []

    if profile.getTwitterAge < profile.TWITTER_REQUIREMENTS['twitter_age']:
        errors.append({
            "title": "Account age",
            "error": "Your account age is {} months. Mininum age is {} months.".format(profile.getTwitterAge, profile.TWITTER_REQUIREMENTS['twitter_age'])
        })

    if profile.twitter_followers < profile.TWITTER_REQUIREMENTS['twitter_followers']:
        errors.append({
            "title": "Followers",
            "error": "You've got only {} of {} minimum required followers.".format(profile.twitter_followers, profile.TWITTER_REQUIREMENTS['twitter_followers'])
        })

    if profile.getTwitterSumOfRepliesAndLikes < profile.TWITTER_REQUIREMENTS['twitter_likes_and_comments']:
        errors.append({
            "title": "Average sum of likes and replies per post during last 3 months",
            "error": "You've got only {} of {} minimum required likes and replies per post.".format(profile.getTwitterSumOfRepliesAndLikes, profile.TWITTER_REQUIREMENTS['twitter_likes_and_comments'])
        })



    return render(request, 'submit_process_template.html', {"errors": errors, "scan_process": profile.scan_process, "scanning_in_progress": scanning_in_progress, "scan_percent": scan_percent, "scan_stage": scan_stage})

@login_required
def special(request):
    return HttpResponse("You are logged in !")

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):

    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse("index"))

    registered = False
    if request.method == 'POST':
        user_form = UserForm(data = request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            user.profile.profile_type = request.POST.get("profile_type")
            user.profile.save()
            login(request, user)

            
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'register.html', {'user_form':user_form, 'registered':registered})
    else:
        user_form = UserForm()
        return render(request, 'register.html', {'user_form':user_form, 'registered':registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            invalid = True
            return render(request, 'login.html', {'invalid':invalid})
    else:
        return render(request, 'login.html', {})

@login_required
def add_socials(request):
    if request.method == 'POST' and request.user.is_authenticated:
        user_socials_form = UserSocialsForm(request.POST)
        user_social = UserSocials.objects.filter(user=request.user)
        if "twitter.com" in str(request.POST['user_twitter']):
            if user_social:
                user_social = user_social[0]
                user_social.user_twitter = request.POST['user_twitter']
                user_social.user_facebook = request.POST['user_facebook']
                data = parse_tweet(request.POST['user_twitter'])
                print(data['twitter_followers'])
                print(data['tw_reg_date'])
                print(data['tw_likes'])
                user_social.user_tw_followers = data['twitter_followers']
                user_social.user_tw_likes = data['tw_likes']
                user_social.user_tw_reg_date = data['tw_reg_date']
                user_social.save(update_fields = ['user_twitter', 'user_facebook', 'user_tw_followers', 'user_tw_likes', 'user_tw_reg_date'])
            else:
                if user_socials_form.is_valid():
                    user_socials_form.user_twitter = request.POST['user_twitter']
                    user_socials_form.user_facebook = request.POST['user_facebook']
                    profile = user_socials_form.save(commit=False)
                    data = parse_tweet(request.POST['user_twitter'])
                    print(data['twitter_followers'])
                    print(data['tw_reg_date'])
                    print(data['tw_likes'])
                    profile.user_tw_followers = data['twitter_followers']
                    profile.user_tw_likes = data['tw_likes']
                    profile.user_tw_reg_date = data['tw_reg_date']
                    profile.user = request.user
                    profile.save()

        # user_socials_form.save()
        return render(request, 'socials.html', {'user_socials_form':user_socials_form})

    elif request.user.is_authenticated:
        user_socials_form = UserSocialsForm(request.POST)
        return render(request, 'socials.html', {'user_socials_form':user_socials_form})

    else:
        return HttpResponse("You are not logged in!")

    return render(request, 'socials.html', {'user_socials_form':user_socials_form})

@login_required
def join_campaign(request, campaign_n):
    campaign = Campaign.objects.get(pk=campaign_n)

    campaign = campaign.join_campaign(request.user)

    return HttpResponseRedirect('/campaigns/{}'.format(campaign.campaign_name))

@login_required
def quit_campaign(request, campaign_n):
    campaign = Campaign.objects.get(pk=campaign_n)

    campaign = campaign.quit_campaign(request.user)

    return HttpResponseRedirect('/campaigns/{}'.format(campaign.campaign_name))



@login_required
def setlike(request):
    id = request.POST.get('id', 0)
    social = request.POST.get('social', 'tw')

    post = Post.objects.filter(pk=int(id)).first()

    if post:
        if post.like_price > 0:
            status, created = PostShareStatus.objects.get_or_create(post=post, user=request.user, social=social, share_type=2)
            if created:
                status.status = 1
                status.save()

        if post.share_price > 0:
            status, created = PostShareStatus.objects.get_or_create(post=post, user=request.user, social=social, share_type=3)
            if created:
                status.status = 1
                status.save()

        if post.comment_price > 0:
            status, created = PostShareStatus.objects.get_or_create(post=post, user=request.user, social=social, share_type=4)
            if created:
                status.status = 1
                status.save()

    return HttpResponse("Ok!")

@login_required
def maketweet(request):
    id = request.POST.get('id', 0)
    social = request.POST.get('social', 'tw')

    post = Post.objects.filter(pk=int(id)).first()

    if post:
        status, created = PostShareStatus.objects.get_or_create(post=post, user=request.user, social=social, share_type=1)
        if created:
            status.status = 1
            status.save()

    return HttpResponse("Ok!")

@login_required
@check_submit_profile
def campaigns(request):
    campaigns = None
    if request.user.profile.profile_type == 1:
        user_campaigns = Users_Campaigns.objects.filter(user=request.user, active=True).values("campaign")
        campaigns = Campaign.objects.exclude(pk__in=user_campaigns)
        your_campaigns = Campaign.objects.filter(pk__in=user_campaigns)
    else:
        your_campaigns = Campaign.objects.filter(creator=request.user)
    
    return render(request, 'campaigns.html', {'user': request.user, 'join_campaigns':campaigns, 'your_campaigns':your_campaigns})

@login_required
def update_campaign(request, campaign_n):
    actions = []
    errors = {}
    campaign = Campaign.objects.get(pk=campaign_n)
    campaign_form = CampaignForm()
    

    if request.method == 'POST':
        #raise Exception(str(request.POST))
        campaign_form = CampaignForm(data = request.POST)
        errors = campaign_form.errors
        #raise Exception(str(errors))
       
        if campaign_form.is_valid():
            campaign_data = campaign_form.cleaned_data
            actions = campaign_data.get("actions")
            del(campaign_data['actions'])

            update_from_dict(campaign, campaign_data)
            campaign.save()

            for action in actions:
                action_data = action.cleaned_data
                #raise Exception(str(action_data))

                if int(action_data['post_id']) > 0:
                    post = Post.objects.get(pk=action_data['post_id'])
                else:
                    post = Post()
                    post.campaign = campaign

                update_from_dict(post, action_data)
                
                post.save()
            return JsonResponse({
                "success": True
            })



            #campaign_form.save()
        else:
            resp = JsonResponse({
                "success": False,
                "errors": errors
            }, status=400)

            return resp
            



    actions = campaign.posts.all()
    success_url = None

    

    return render(request, 'create_campaign.html', {"success_url": success_url, 'user': request.user, "campaign_form": campaign_form, 'campaign': campaign, 'actions': actions, "errors": errors})


@login_required
def create_campaign(request):
    actions = []
    errors = {}
    campaign_form = CampaignForm()
    campaign = Campaign()

    if request.method == 'POST':
        campaign_form = CampaignForm(data = request.POST)
        errors = campaign_form.errors
        #raise Exception(str(errors))
       
        if campaign_form.is_valid():
            campaign_data = campaign_form.cleaned_data
            actions = campaign_data.get("actions")
            del(campaign_data['actions'])

            update_from_dict(campaign, campaign_data)
            campaign.creator = request.user
            campaign.save()

            for action in actions:
                action_data = action.cleaned_data
                #raise Exception(str(action_data))
                post = Post()

                update_from_dict(post, action_data)
                post.campaign = campaign
                post.save()
            return JsonResponse({
                "success": True
            })



            #campaign_form.save()
        else:
            resp = JsonResponse({
                "success": False,
                "errors": errors
            }, status=400)

            return resp
            



    for x in range(1,4):
        actions.append(Post())

    success_url = reverse("campaigns")

    return render(request, 'create_campaign.html', {"success_url": success_url, 'user': request.user, "campaign_form": campaign_form, 'campaign': campaign, 'actions': actions, "errors": errors})

@login_required
def add_action(request, last_index):
    return render(request, 'action_template.html', {'user': request.user, 'action': Post(), 'counter': last_index+1})

@login_required
@check_submit_profile
def campaign(request, campaign_n):
    if request.method == 'POST':
        print(request.body)
        return HttpResponse(json.dumps({'kek':'kok'}))
    else:
        campaign = Campaign.objects.get(campaign_name=campaign_n)
        

        user_campaign = Users_Campaigns.objects.filter(user = request.user, campaign=campaign, active=True).first()

        if user_campaign:
            user_campaign.check_compaign_post_results(request.user)
            campaign_already_sharing = True
        else:
            campaign_already_sharing = False


         

        article_posts = Post.objects.filter(campaign=campaign, post_type=1)
        post_posts = Post.objects.filter(campaign=campaign, post_type=2)
        follow_posts = Post.objects.filter(campaign=campaign, post_type=3)
        return render(request, 'campaign.html', {
            'article_posts':article_posts, 
            'post_posts':post_posts, 
            'campaign': campaign,
            'follow_posts': follow_posts, 
            'campaign_already_sharing':campaign_already_sharing
        })

def add_campaign(request):
    if request.method == 'POST':
        campaigns = Users_Campaigns.objects.filter(user = request.user)
        camp_name = get_comp_name(request.body)
        campaign = Campaign.objects.get(campaign_name = camp_name)
        campaign_already_sharing = False
        campaign_on_delete = None
        for camp in campaigns:
            if camp.campaign_id == campaign.id:
                campaign_already_sharing = True
                campaign_on_delete = camp

        if not campaign_already_sharing:
            user = request.user
            user_campaign = Users_Campaigns()
            user_campaign.campaign = campaign
            user_campaign.user = user
            user_campaign.save()
        elif campaign_already_sharing:
            campaign_on_delete.delete()

        return HttpResponse('Campaign added')

def share_campaign(request):
    #if request.method == 'POST':
    campaigns = Users_Campaigns.objects.filter(user = request.user)
    if campaigns:
        profile = UserSocials.objects.filter(user = request.user)
        posts = []
        print(campaigns[0].campaign_id)
        answer = []
        for i in campaigns:
            post = Post.objects.filter(campaign_id = i.campaign_id)
            for j in post:
                posts.append(j)
        for share_post in posts:
            print(check_tweet(share_post.post_link, profile[0].user_twitter))
            answer.append(check_tweet(share_post.post_link, profile[0].user_twitter))
    else:
        answer = ["YOU HAVE NO CAMPAIGNS SHARING"]

    return render(request, 'share.html', {'answer':answer})
