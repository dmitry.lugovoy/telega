import requests
import re
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
import sys
import urllib.request
import threading
from dataclasses import dataclass
import selenium
from selenium import webdriver
import time
from urllib.parse import urlparse, urljoin

def file_log(filename, content):

    with open(filename, 'w') as the_file:
        the_file.write(content)

def do_login(driver, login, password):
     # get the username textbox
    try:
        login_field = driver.find_element_by_class_name("js-username-field")
        if not login_field:
            print("Login not found")
            return driver
    except:
        print("Login not found")
        return driver

    login_field.clear()

    # enter username
    login_field.send_keys(login)
    time.sleep(1)

    #get the password textbox
    password_field = driver.find_element_by_class_name("js-password-field")
    password_field.clear()

    #enter password
    password_field.send_keys(password)
    time.sleep(1)

    file_log("before_submit.html", driver.page_source)
    password_field.submit()

    time.sleep(3)

    file_log("after_submit.html", driver.page_source)

    return driver

def twitter_login(driver):
    print("Loggin in to twitter")
    driver.implicitly_wait(3)
    driver.maximize_window()
    driver.delete_all_cookies()
    login = "dmitry.grassman@gmail.com"
    password = "nasyrovaastral123"

    # navigate to the application home page
    driver.get("https://twitter.com/login")




    driver = do_login(driver, login, password)
    

    time.sleep(2)

    driver = do_login(driver, login, password)


    


    

    return driver



def update_from_dict(instance, attrs, commit=False):
    allowed_field_names = {
        f.name for f in instance._meta.get_fields()
        if True
    }

    for attr, val in attrs.items():
        if attr in allowed_field_names:
            setattr(instance, attr, val)

    if commit:
        instance.save()

def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def get_selenium_contents(chrome, url, query, **kwargs):
    import datetime
    from selenium.webdriver.support.ui import WebDriverWait 
    import pytz
    

    target_scroll_times = kwargs.get("scroll_to_bottom", 0)
    compare = kwargs.get("compare", None)
    login = kwargs.get("login", True)
    time_compare = kwargs.get("time_compare", None)
    scroll_element = kwargs.get("scroll_element", None)

    if not scroll_element:
        scroll_window = "window"
        scroll_body = "document.body"
    else:
        scroll_window = scroll_element
        scroll_body = scroll_element

    scrolled = 0


    
    #chrome = webdriver.Chrome("./chromedriver")

    if login:
        chrome = twitter_login(chrome)
    print("Getting elements "+url)
    chrome.get(url)
    delay = 3 # seconds

    source = ""
    time.sleep(delay)
    elements = []
    found_elements = []
    last_elements = []


    if target_scroll_times > 0:
        SCROLL_PAUSE_TIME = 2

        # Get scroll height
        script = "return {scroll_body}.scrollHeight".format(scroll_body=scroll_body)
        print("Executing: "+script)
        last_height = chrome.execute_script(script)

        while True:
            # Scroll down to bottom
            print("Searching elements with "+query)
            elements = list(chrome.find_elements_by_css_selector(query))

            elements = [x for x in elements if x not in last_elements]


            
            print("Done searching. Length is {}".format(len(elements)))

            if len(elements) and compare:

                for user in elements:
                    link = urlparse(user.get_attribute('href')).path.lower()
                    print("Link is {link} comparing to {compare}".format(link=link, compare=compare))

                    if link == compare:
                        print("Found user!")
                        return [user]

            if len(elements) and time_compare:

                for el in elements:
                    

                    #time.sleep(SCROLL_PAUSE_TIME)
                    try:
                        _time = el.get_attribute('datetime')
                        _time = datetime.datetime.strptime(_time, "%Y-%m-%dT%H:%M:%S.%fZ")
                        

                        print("Time is {time} comparing to {time_compare}".format(time=_time, time_compare=time_compare))

                        if _time > time_compare:
                            parent = chrome.find_element_by_xpath(".//ancestor::article")
                            print("Appending {parent} to found elements ".format(parent=parent))
                            found_elements.append(parent.get_attribute('innerHTML'))
                            #return found_elements
                        else:
                            return found_elements
                    except:
                        print("Element {} is not displayed".format(el))
                        continue
                        
                        
                
            script = "{scroll_window}.scrollTo(0, {scroll_body}.scrollHeight);".format(scroll_body=scroll_body, scroll_window=scroll_window)
            print("Executing: "+script)
            chrome.execute_script(script)

            # Wait to load page
            time.sleep(SCROLL_PAUSE_TIME)
            print("Scrolling down")

            # Calculate new scroll height and compare with last scroll height
            script = "return {scroll_body}.scrollHeight".format(scroll_body=scroll_body)
            print("Executing: "+script)
            new_height = chrome.execute_script(script)
            if new_height == last_height or scrolled >= target_scroll_times:
                print("At the bottom")
                time.sleep(delay)
                break
            last_height = new_height
            scrolled+=1
            last_elements = elements
    else:
        elements = chrome.find_elements_by_css_selector(query)

    print("Done getting elements")
    #print(els)
   

    if len(found_elements):
        return found_elements


    return elements

def add_cookies(driver):

    cookies = []

    driver.get("http://www.example.com/this404page")

    cookies = [{"domain":".twitter.com","expiry":1634214797,"hostOnly":False,"httpOnly":False,"name":"_ga","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"GA1.2.1512376388.1569682023","id":1},{"domain":".twitter.com","expiry":1571142857,"hostOnly":False,"httpOnly":False,"name":"_gat","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"1","id":2},{"domain":".twitter.com","expiry":1571229197,"hostOnly":False,"httpOnly":False,"name":"_gid","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"GA1.2.562113496.1571135759","id":3},{"domain":".twitter.com","hostOnly":False,"httpOnly":True,"name":"_twitter_sess","path":"/","sameSite":"unspecified","secure":True,"session":True,"storeId":"0","value":"BAh7CiIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7ADoPY3JlYXRlZF9hdGwrCBPwQc9tAToMY3NyZl9p%250AZCIlMjlkYjQ2OTViNTM1NDY3ZWU1ZDQ3ZWZiYzRhYzA2OTg6B2lkIiVkMjkw%250AM2ZkMGIzYjExMzIyMWY2MGRjM2Y3NGI0ZGJhMToJdXNlcmwrCQFQl4hG%252B1kQ--c8c4cc14951b088f1d61fd0106059cf2b9578445","id":4},{"domain":".twitter.com","expiry":1728822617.521409,"hostOnly":False,"httpOnly":False,"name":"ads_prefs","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"\"HBISAAA=\"","id":5},{"domain":".twitter.com","hostOnly":False,"httpOnly":True,"name":"auth_token","path":"/","sameSite":"unspecified","secure":True,"session":True,"storeId":"0","value":"029c110e6ea66a43e083031c757f8f34da9b3bb0","id":6},{"domain":".twitter.com","expiry":1601287489.765102,"hostOnly":False,"httpOnly":True,"name":"csrf_same_site","path":"/","sameSite":"lax","secure":True,"session":False,"storeId":"0","value":"1","id":7},{"domain":".twitter.com","expiry":1601201089.765074,"hostOnly":False,"httpOnly":True,"name":"csrf_same_site_set","path":"/","sameSite":"unspecified","secure":True,"session":False,"storeId":"0","value":"1","id":8},{"domain":".twitter.com","expiry":1571157357.225777,"hostOnly":False,"httpOnly":False,"name":"ct0","path":"/","sameSite":"unspecified","secure":True,"session":False,"storeId":"0","value":"fc9342fb1cc0c09e12dff645789f8176","id":9},{"domain":".twitter.com","expiry":1728822617.745117,"hostOnly":False,"httpOnly":False,"name":"dnt","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"1","id":10},{"domain":".twitter.com","expiry":1571745622.354054,"hostOnly":False,"httpOnly":False,"name":"external_referer","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"wtDTd9sweTcWWA%2FmJaNTO5BStu3C18RXK7UO9iH61BoPE2qmvDGnKwS4moN%2B5yYqxbHM%2F3%2F3CFaupkufCfIk4w%3D%3D|0|8e8t2xd8A2w%3D","id":11},{"domain":".twitter.com","expiry":1571147106.383455,"hostOnly":False,"httpOnly":False,"name":"gt","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"1184057499827294210","id":12},{"domain":".twitter.com","expiry":1634212292.959877,"hostOnly":False,"httpOnly":False,"name":"guest_id","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"v1%3A157114026804995142","id":13},{"domain":".twitter.com","expiry":1618403417.521429,"hostOnly":False,"httpOnly":True,"name":"kdt","path":"/","sameSite":"unspecified","secure":True,"session":False,"storeId":"0","value":"gIFRz1lj2odHqARR487GcOhLRrSAogxjsdfINFLb","id":14},{"domain":".twitter.com","expiry":1634212292.959861,"hostOnly":False,"httpOnly":False,"name":"personalization_id","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"\"v1_FIClPdxAbskIfbIL7mX84w==\"","id":15},{"domain":".twitter.com","expiry":1728822617.521466,"hostOnly":False,"httpOnly":False,"name":"remember_checked_on","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"0","id":16},{"domain":".twitter.com","expiry":1602678617.745239,"hostOnly":False,"httpOnly":False,"name":"rweb_optin","path":"/","sameSite":"unspecified","secure":True,"session":False,"storeId":"0","value":"side_no_out","id":17},{"domain":".twitter.com","expiry":1572214317.733436,"hostOnly":False,"httpOnly":False,"name":"tfw_exp","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"0","id":18},{"domain":".twitter.com","expiry":1728822795.22504,"hostOnly":False,"httpOnly":False,"name":"twid","path":"/","sameSite":"unspecified","secure":True,"session":False,"storeId":"0","value":"u%3D1178249057891995649","id":19},{"domain":"twitter.com","expiry":1571226369,"hostOnly":True,"httpOnly":False,"name":"_sl","path":"/","sameSite":"unspecified","secure":False,"session":False,"storeId":"0","value":"1","id":20},{"domain":"twitter.com","hostOnly":True,"httpOnly":False,"name":"lang","path":"/","sameSite":"unspecified","secure":False,"session":True,"storeId":"0","value":"en","id":21}]

    for cookie in cookies :
        driver.add_cookie({k: cookie.get(k) for k in ('name', 'value', 'domain', 'path')})

    return driver

def get_driver():
    from selenium import webdriver
    desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()

    options = webdriver.ChromeOptions()
    ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
    
    options.add_argument('--headless')
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-gpu")
    options.add_argument("--remote-debugin-port=9222")
    options.add_argument("--lang=en");
    options.add_argument("--screen-size=1920x1080")
    options.add_argument('--log-level=3')
    options.add_argument('--user-agent={}'.format(ua))
    chrome = webdriver.Remote("http://selenium-hub:4444/wd/hub", desired_capabilities=options.to_capabilities())
    chrome.delete_all_cookies()
    chrome = add_cookies(chrome)

    return chrome

def get_contents(url):
    """Scrape URLs to generate previews."""
    print("Getting contents: "+url)
    headers = requests.utils.default_headers()
    headers.update({
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
        'accept-language': 'en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7'
    })
    r = requests.get(url, headers)

    raw_html = r.content.decode('utf8')
    return raw_html

def scrape(url):
    """Scrape URLs to generate previews."""
   
    raw_html = get_contents(url)
    soup = BeautifulSoup(raw_html)
    return soup

def parse_short_url(shorturl):
    #shorturl = raw_input("Enter the shortened URL in its entirety: ")
    r = requests.get(shorturl)

    return r.url


@dataclass
class Data:
    twitter_followers: str
    tw_reg_date: str
    tw_likes: str


class ParseThread(threading.Thread):
    def __init__(self, soup, function, *args, **kwargs):
        self.soup = soup
        self.function = function
        super(ParseThread, self).__init__(*args, **kwargs)

    def run(self):
            Data.answer = self.function(self.soup)


def get_twitter_followers(soup):
    # client_response = Client(url)
    # soup = BeautifulSoup(client_response.html,'html.parser')
    obj = soup.find('li', attrs = {'class':'ProfileNav-item ProfileNav-item--followers'})
    obj = obj.find('span', attrs = {'class':'ProfileNav-value'})
    answer = []
    for i in str(obj):
        if str(i) == ">":
            break
        if str(i).isdigit():
            answer.append(i)
    Data.twitter_followers = "".join(answer)


def get_tw_reg_date(soup):
    # client_response = Client(url)
    # soup = BeautifulSoup(client_response.html,'html.parser')
    obj = soup.find('div', attrs = {'class':'ProfileHeaderCard-joinDate'})
    answer = str(obj.find('span', attrs = {'dir':'ltr'})).split('title')[-1]
    what = []
    for i in answer:
        if i.isdigit() or i.isalpha():
            what.append(i)
        if i == ">":
            break
    reg_date = ''.join(what)[4:-1:1]
    Data.tw_reg_date = reg_date


def get_tw_likes(soup):
    # client_response = Client(client_response)
    # soup = BeautifulSoup(client_response.html,'html.parser')
    obj = soup.find_all('span', attrs = {'class':'ProfileTweet-actionCountForAria'})
    answer = []
    for i in range(len(obj)-1):
        if 'favorite' in str(obj[i]):
            answer.append(obj[i])
    # likes = []
    # for i in range(len(answer)-1):
    #         likes.append(str(answer[i]).split('>')[-2].split('отмет')[0].replace(u'\xa0', ''))
    mean_value = 0
    for i in range(len(answer)-1):
        mean_value += int(str(answer[i]).split('>')[-2].split('отмет')[0].replace(u'\xa0', ''))
    if mean_value > 0:
        like = mean_value / len(answer)
    else:
        like = 0
    Data.tw_likes = like


def parse_tweet(url):
    options = selenium.webdriver.ChromeOptions()
    options.add_argument('headless')
    #executable_path=os.path.abspath("chromedriver.exe")
    driver = webdriver.Chrome()
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, 'lxml')
    t1 = ParseThread(soup, get_twitter_followers)
    t2 = ParseThread(soup, get_tw_reg_date)
    t3 = ParseThread(soup, get_tw_likes)
    t1.start()
    t2.start()
    t3.start()
    t1.join()
    t2.join()
    t3.join()
    driver.quit()


    return {'twitter_followers':Data.twitter_followers, 'tw_reg_date':Data.tw_reg_date, 'tw_likes':Data.tw_likes}


def check_tweet(url_tw, twitter):
    options = selenium.webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome()
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, 'lxml')
    target = url_tw.split('/')[-1]
    obj = soup.find_all('div', attrs = {'data-conversation-id':target})
    answer = True
    if len(obj) == 0:
        answer = False
    return answer
    driver.quit()


def get_comp_name(url):
    answer = str(url).split('/')[-1]
    return str(answer)[0:-3:1]
