from functools import wraps
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.urls import reverse
import functools

def check_submit_profile(fn):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """
    # def actual_decorator(func):
    #     @functools.wraps(func)
    #     def wrapper(*args, **kwargs):
            
    #         return func(*args, **kwargs)
    #     return wrapper
    # return actual_decorator

    def wrapped(*args, **kwargs):

        request = args[0]
        
        user = request.user
        if user.is_authenticated and user.profile.profile_type == 1:
            if not user.profile.twitter_account or not user.profile.isTwitterOk:
                return HttpResponseRedirect(reverse("profile_submit"))
        return fn(*args, **kwargs)
    return wrapped