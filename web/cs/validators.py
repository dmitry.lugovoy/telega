from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def twitter_validator(value):

	import re
	pattern = re.compile("^https:\/\/twitter\.com\/\S+$")
	
	match = pattern.match(value)

	if not match:
		raise ValidationError(
		_('%(value)s is not a correct Twitter url'),
		params={'value': value},
		)

	return True