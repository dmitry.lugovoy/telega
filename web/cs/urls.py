from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('profile/update', views.update_profile, name='update_profile'),
    path('profile/reset_twitter', views.reset_twitter, name='reset_twitter'),
    path('profile/submit', views.profile_submit, name='profile_submit'),
    path('profile/submit_process', views.submit_process_partial, name='submit_process'),
    path('socials/', views.add_socials, name='socials'),
    path('register/', views.register, name = 'register'),
    path('login/', views.user_login, name = 'login'),
    path('accounts/login/', views.user_login, name = 'login'),
    path('logout/', views.user_logout, name = 'logout'),
    path('add_campaign', views.add_campaign, name = 'add_campaign'),
    path('campaigns/', views.campaigns, name = 'campaigns'),
    path('campaigns/add_action/<int:last_index>', views.add_action, name = 'add_action'),
    path('campaigns/create', views.create_campaign, name= 'create_campaign'),
    path('campaigns/update/<int:campaign_n>', views.update_campaign, name= 'update_campaign'),
    path('campaigns/<str:campaign_n>', views.campaign, name = 'campaign'),
    path('campaigns/join/<int:campaign_n>', views.join_campaign, name = 'join_campaign'),
    path('campaigns/quit/<int:campaign_n>', views.quit_campaign, name = 'quit_campaign'),
    path('posts/maketweet/', views.maketweet, name = 'setstatus'),
    path('posts/setlike/', views.setlike, name = 'setlike'),
    path('share', views.share_campaign, name = 'share'),
]
